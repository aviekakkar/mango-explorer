{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "honest-cleaners",
   "metadata": {},
   "source": [
    "# ⚠ Warning\n",
    "\n",
    "THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/OpinionatedGeek%2Fmango-explorer/HEAD?filepath=AccountScout.ipynb) _🏃‍♀️ To run this notebook press the ⏩ icon in the toolbar above._\n",
    "\n",
    "[🥭 Mango Markets](https://mango.markets/) support is available at: [Docs](https://docs.mango.markets/) | [Discord](https://discord.gg/67jySBhxrg) | [Twitter](https://twitter.com/mangomarkets) | [Github](https://github.com/blockworks-foundation) | [Email](mailto:hello@blockworks.foundation)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "complete-documentation",
   "metadata": {},
   "source": [
    "# 🥭 AccountScout\n",
    "\n",
    "If you want to run this code to check a user account, skip down to the **Running** section below and follow the instructions there.\n",
    "\n",
    "(A 'user account' here is the root SOL account for a user - the one you have the private key for, and the one that owns sub-accounts.)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "whole-valentine",
   "metadata": {},
   "source": [
    "## Required Accounts\n",
    "\n",
    "Mango Markets code expects some accounts to be present, and if they're not present some actions can fail.\n",
    "\n",
    "From [Daffy on Discord](https://discord.com/channels/791995070613159966/820390560085835786/834024719958147073):\n",
    "\n",
    "> You need an open orders account for each of the spot markets Mango. And you need a token account for each of the tokens.\n",
    "\n",
    "This notebook (and the `AccountScout` class) can be used to check the required accounts are present and maybe in future set up all the required accounts.\n",
    "\n",
    "(There's no reason not to write the code to fix problems and create any missing accounts. It just hasn't been done yet.)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "monthly-elephant",
   "metadata": {
    "jupyter": {
     "source_hidden": true
    }
   },
   "outputs": [],
   "source": [
    "import logging\n",
    "import typing\n",
    "\n",
    "from solana.publickey import PublicKey\n",
    "\n",
    "from BaseModel import Group, MarginAccount\n",
    "from Constants import SYSTEM_PROGRAM_ADDRESS\n",
    "from Context import Context\n",
    "from Wallet import Wallet\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "strong-logic",
   "metadata": {},
   "source": [
    "# ScoutReport class\n",
    "\n",
    "The `ScoutReport` class is built up by the `AccountScout` to report errors, warnings and details pertaining to a user account."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "unknown-collective",
   "metadata": {},
   "outputs": [],
   "source": [
    "class ScoutReport:\n",
    "    def __init__(self, address: PublicKey):\n",
    "        self.address = address\n",
    "        self.errors: typing.List[str] = []\n",
    "        self.warnings: typing.List[str] = []\n",
    "        self.details: typing.List[str] = []\n",
    "\n",
    "    @property\n",
    "    def has_errors(self) -> bool:\n",
    "        return len(self.errors) > 0\n",
    "\n",
    "    @property\n",
    "    def has_warnings(self) -> bool:\n",
    "        return len(self.warnings) > 0\n",
    "\n",
    "    def add_error(self, error) -> None:\n",
    "        self.errors += [error]\n",
    "\n",
    "    def add_warning(self, warning) -> None:\n",
    "        self.warnings += [warning]\n",
    "\n",
    "    def add_detail(self, detail) -> None:\n",
    "        self.details += [detail]\n",
    "\n",
    "    def __str__(self) -> str:\n",
    "        def _pad(text_list: typing.List[str]) -> str:\n",
    "            if len(text_list) == 0:\n",
    "                return \"None\"\n",
    "            padding = \"\\n        \"\n",
    "            return padding.join(map(lambda text: text.replace(\"\\n\", padding), text_list))\n",
    "\n",
    "        error_text = _pad(self.errors)\n",
    "        warning_text = _pad(self.warnings)\n",
    "        detail_text = _pad(self.details)\n",
    "        if len(self.errors) > 0 or len(self.warnings) > 0:\n",
    "            summary = f\"Found {len(self.errors)} error(s) and {len(self.warnings)} warning(s).\"\n",
    "        else:\n",
    "            summary = \"No problems found\"\n",
    "\n",
    "        return f\"\"\"« ScoutReport [{self.address}]:\n",
    "    Summary:\n",
    "        {summary}\n",
    "\n",
    "    Errors:\n",
    "        {error_text}\n",
    "\n",
    "    Warnings:\n",
    "        {warning_text}\n",
    "\n",
    "    Details:\n",
    "        {detail_text}\n",
    "»\"\"\"\n",
    "\n",
    "    def __repr__(self) -> str:\n",
    "        return f\"{self}\"\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "turned-passing",
   "metadata": {},
   "source": [
    "# AccountScout class\n",
    "\n",
    "The `AccountScout` class aims to run various checks against a user account to make sure it is in a position to run the liquidator.\n",
    "\n",
    "Passing all checks here with no errors will be a precondition on liquidator startup."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "literary-development",
   "metadata": {},
   "outputs": [],
   "source": [
    "class AccountScout:\n",
    "    def __init__(self):\n",
    "        pass\n",
    "\n",
    "    def require_account_prepared_for_group(self, context: Context, group: Group, account_address: PublicKey) -> None:\n",
    "        report = self.verify_account_prepared_for_group(context, group, account_address)\n",
    "        if report.has_errors:\n",
    "            raise Exception(f\"Account '{account_address}' is not prepared for group '{group.address}':\\n\\n{report}\")\n",
    "\n",
    "    def verify_account_prepared_for_group(self, context: Context, group: Group, account_address: PublicKey) -> ScoutReport:\n",
    "        report = ScoutReport(account_address)\n",
    "\n",
    "        # First of all, the account must actually exist. If it doesn't, just return early.\n",
    "        root_account = context.load_account(account_address)\n",
    "        if root_account is None:\n",
    "            report.add_error(f\"Root account '{account_address}' does not exist.\")\n",
    "            return report\n",
    "\n",
    "        # For this to be a user/wallet account, it must be owned by 11111111111111111111111111111111.\n",
    "        if root_account.owner != SYSTEM_PROGRAM_ADDRESS:\n",
    "            report.add_error(f\"Account '{account_address}' is not a root user account.\")\n",
    "            return report\n",
    "\n",
    "        # Must have token accounts for each of the tokens in the group's basket.\n",
    "        for token in group.tokens:\n",
    "            token_account  = context.fetch_token_balance(account_address, token.mint)\n",
    "            if token_account is None:\n",
    "                report.add_error(f\"Account '{account_address}' has no account for token '{token.name}', mint '{token.mint}'.\")\n",
    "            else:\n",
    "                report.add_detail(f\"Token account mint '{token.mint}' balance: {token_account} {token.name}\")\n",
    "\n",
    "        # Must have at least one Mango Markets margin account.\n",
    "        margin_accounts = MarginAccount.load_all_for_owner(context, context.program_id, group, account_address)\n",
    "        if len(margin_accounts) == 0:\n",
    "            report.add_error(f\"Account '{account_address}' has no Mango Markets margin accounts.\")\n",
    "        else:\n",
    "            for margin_account in margin_accounts:\n",
    "                report.add_detail(f\"Margin account: {margin_account}\")\n",
    "\n",
    "        # At least one Mango Markets margin account must have OpenOrders accounts for all the\n",
    "        # Group's markets. There can be more than one margin account per account, but we only\n",
    "        # need one of them to be complete.\n",
    "        margin_account_with_all_market_openorders: MarginAccount = None\n",
    "        oo_errors = []\n",
    "        for margin_account in margin_accounts:\n",
    "            all_markets_available = True\n",
    "            for index, open_orders_address in enumerate(margin_account.open_orders):\n",
    "                if open_orders_address == SYSTEM_PROGRAM_ADDRESS:\n",
    "                    all_markets_available = False\n",
    "                    report.add_warning(f\"Account '{account_address}', MarginAccount '{margin_account.address}' has no OpenOrders account for market {group.markets[index].name}, address '{group.markets[index].address}'.\")\n",
    "                    oo_errors += [f\"Account '{account_address}', MarginAccount '{margin_account.address}' has no OpenOrders account for market {group.markets[index].name}, address '{group.markets[index].address}'.\"]\n",
    "\n",
    "            if all_markets_available:\n",
    "                margin_account_with_all_market_openorders = margin_account\n",
    "                break\n",
    "\n",
    "        # If we didn't find any Mango Markets margin account with OpenOrders for each market,\n",
    "        # report what is lacking in each margin account.\n",
    "        #\n",
    "        # If we did find one suitable margin account, ignore any problems with any other\n",
    "        # margin accounts.\n",
    "        if margin_account_with_all_market_openorders is None:\n",
    "            map(report.add_error, oo_errors)\n",
    "\n",
    "        return report\n",
    "\n",
    "    # It would be good to be able to fix an account automatically, which should\n",
    "    # be possible if a Wallet is passed.\n",
    "    def prepare_wallet_for_group(self, wallet: Wallet, group: Group) -> ScoutReport:\n",
    "        report = ScoutReport(wallet.address)\n",
    "        report.add_error(\"AccountScout can't yet prepare wallets.\")\n",
    "        return report\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "rapid-daniel",
   "metadata": {},
   "source": [
    "# 🏃 Running\n",
    "\n",
    "You can run the following cell to check any user account to make sure it has the proper sub-accounts set up and available.\n",
    "\n",
    "Enter the public key of the account you want to verify in the value for `ACCOUNT_TO_VERIFY` in the box below, between the double-quote marks. Then run the notebook by choosing 'Run > Run All Cells' from the notebook menu at the top of the page."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "skilled-senate",
   "metadata": {},
   "outputs": [],
   "source": [
    "ACCOUNT_TO_VERIFY = \"\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "martial-librarian",
   "metadata": {},
   "outputs": [],
   "source": [
    "if __name__ == \"__main__\":\n",
    "    logging.basicConfig(level=logging.DEBUG)\n",
    "\n",
    "    if ACCOUNT_TO_VERIFY == \"\":\n",
    "        raise Exception(\"No account to look up - try setting the variable ACCOUNT_TO_LOOK_UP to an account public key.\")\n",
    "\n",
    "    from Context import default_context\n",
    "\n",
    "    print(\"Context:\", default_context);\n",
    "\n",
    "    root_account_key = PublicKey(ACCOUNT_TO_VERIFY)\n",
    "    group = Group.load(default_context)\n",
    "\n",
    "    scout = AccountScout()\n",
    "    report = scout.verify_account_prepared_for_group(default_context, group, root_account_key)\n",
    "\n",
    "    print(report)\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
